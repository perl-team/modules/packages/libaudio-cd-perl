/*
cdcd - Command Driven CD player
Copyright (C)1998-99 Tony Arcieri

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/* modified for Audio::CD by dougm */

typedef int (*cddb_inexact_selection_func_t)(void);

void cddb_lookup(int cd_desc, struct disc_data *data);
void cddb_verbose(void *h, int flag);
void cddb_inexact_selection_set(cddb_inexact_selection_func_t func);
