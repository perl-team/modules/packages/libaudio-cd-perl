# Spec file for the Audio::CD perl package
#
%define vers 0.05
#
Summary: perl Audio::CD module
Name: perl-Audio-CD
Version: %{vers}
Requires: perl >= 5.003, libcdaudio >= 0.99.4
Release: 1s
Copyright: Perl
Group: Development/Libraries
Source: http://chinstrap.cylant.com/~stephen/Audio-CD-0.04-stephen.tar.gz
URL: http://search.cpan.org/search?module=Audio::CD
BuildArchitectures: i386
Buildroot: /tmp/perl-audio-cd

%description
Perl Audio::CD is a perl interface to libcdaudio (cd + cddb) 

%prep
%setup -q -n Audio-CD-0.04-stephen

%build

#get perl's version
eval "perl_`perl '-V:version'`"
eval "perl_`perl '-V:installman1dir'`"
eval "perl_`perl '-V:installman3dir'`"
eval "perl_`perl '-V:man1ext'`"
eval "perl_`perl '-V:man3ext'`"

if test  $perl_version = '5.00503'  ; then
        perl_prefix=''
        perl_pollute=''
elif test $perl_version = '5.6.0' ; then
        perl_prefix=$RPM_BUILD_ROOT
        perl_pollute="POLLUTE=1"
else
        perl_perfix=$RPM_BUILD_ROOT
        perl_pollute="POLLUTE=1"
fi

#Some modules require POLLUTE=1 in the next line
CFLAGS="$RPM_OPT_FLAGS" perl Makefile.PL \
INSTALLMAN1DIR=$perl_prefix$perl_installman1dir \
INSTALLMAN3DIR=$perl_prefix$perl_installman3dir \
#$perl_pollute


make OPTIMIZE="${RPM_OPT_FLAGS}"
make test





%install
rm -rf $RPM_BUILD_ROOT

eval `perl '-V:installarchlib'`
eval "perl_`perl '-V:version'`"
eval "perl_`perl '-V:installman1dir'`"
eval "perl_`perl '-V:installman3dir'`"
eval "perl_`perl '-V:man1ext'`"
eval "perl_`perl '-V:man3ext'`"

mkdir -p $RPM_BUILD_ROOT$installarchlib
mkdir -p $RPM_BUILD_ROOT/usr/doc/%{name}-%{version}
mkdir -p $perl_installman1dir
mkdir -p $perl_installman3dir

make PREFIX=$RPM_BUILD_ROOT/usr install

#Get the file "perllocal.pod" location
find $RPM_BUILD_ROOT/usr -type f -print | grep perllocal.pod > podfile_full.lst

#Correct the data inside the file
mv `cat podfile_full.lst` `cat podfile_full.lst`.tmp
cat `cat podfile_full.lst`.tmp | sed -e "s@$RPM_BUILD_ROOT@@g" > `cat podfile_full.lst`
rm `cat podfile_full.lst`.tmp

#move it to docs
mv `cat podfile_full.lst` $RPM_BUILD_DIR/%{buildsubdir}

#strips
cp podfile_full.lst podfile_full2.lst
cat podfile_full2.lst | sed -e "s@^$RPM_BUILD_ROOT@@" > podfile_full.lst
#Get the filelist     \
#Add gz to man pages
find $RPM_BUILD_ROOT/usr -type f -print |\
  sed -e "s@^$RPM_BUILD_ROOT@@g" |\
  sed -e "s@\($perl_installman1dir/.*\.$perl_man1ext\)\$@\1\.gz@" |\
  sed -e "s@\($perl_installman3dir/.*\.$perl_man3ext\)\$@\1\.gz@" |\
 grep -v perllocal.pod > file-list.lst

mkdir -p $RPM_BUILD_ROOT/usr/lib/perl5/perllocal
cp perllocal.pod $RPM_BUILD_ROOT/usr/lib/perl5/perllocal/%{name}-perllocal.pod
cp podfile_full.lst $RPM_BUILD_ROOT/usr/lib/perl5/perllocal/%{name}-podfile_full.lst




%clean
rm -rf $RPM_BUILD_DIR/Audio-CD-0.04-stephen
rm -rf $RPM_BUILD_ROOT
rm -f file-list.lst
rm -f podfile_full.lst

%files -f file-list.lst
%defattr(-,root,root)
%docdir /usr/doc/%{name}-%{version} 
%doc README
%doc MANIFEST
%doc eg/
/usr/lib/perl5/perllocal/%{name}-perllocal.pod
/usr/lib/perl5/perllocal/%{name}-podfile_full.lst




%post

#Adjusts the date too
cat /usr/lib/perl5/perllocal/%{name}-perllocal.pod |\
sed -e "s@\(^=head2 \).*\(:.*\)@\1`date`\2@" >> `cat /usr/lib/perl5/perllocal/%{name}-podfile_full.lst`

